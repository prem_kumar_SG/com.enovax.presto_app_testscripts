package com.enovax.Test_Scripts;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.apache.commons.lang3.time.DurationFormatUtils;
//import org.apache.poi.ss.formula.functions.Column;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestContext;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.enovax.Test_Scripts.Application_TestCases.ClsTestCases;
import com.enovax.Test_Scripts.Application_Utilities.Application_Utilities;
import com.enovax.Test_Scripts.Application_Utilities.ExcelLib;

import API_Implementation.API;
import Utills.DomainList;
import Utills.ExcelUtils;
import Utills.Report;
import ObjectRepository.OR;
import io.appium.java_client.AppiumDriver;

public class Demo {
	
	private WebDriver driver;
	private API API;
	private OR OR;
	private String URL;
	private long start;
	String waitingTime;	
	String browsername;
	String prevBrowserName;
	int iResultColumn = 3;
	final String PASS_KEYWORD = "Pass", FAIL_KEYWORD = "Fail"; 
	
	@BeforeClass
	public void setupSelenium() throws Exception {
		waitingTime=AppDriver.getWaitingTime();
	}

	@BeforeMethod
	public void beforeTest() throws InterruptedException{
		start = Reporter.getCurrentTestResult().getStartMillis();
	}
	
	@AfterMethod
	public void afterTest(){
		long end = Reporter.getCurrentTestResult().getEndMillis();
		String execTime =DurationFormatUtils.formatDurationHMS((end-start));
		System.out.println("Total Test Execution time - millisec: " + execTime);
		System.out.println("Total Test Execution time - HMS: " + DurationFormatUtils.formatDurationHMS((end-start)));
	}
	
		
	@Test(dataProviderClass=Utills.DomainDataProvider.class,dataProvider="DomainListData")
    public void fnTest(DomainList domainListItem) throws UnknownHostException, MalformedURLException, InterruptedException {
		
		browsername = domainListItem.getBrowserName();
		AppDriver.createWebDriverObject(browsername);
		driver=AppDriver.getDriverObject();
	    API = PageFactory.initElements(driver, API.class);
		OR = PageFactory.initElements(driver,OR.class);
		
		Report.screenShotFolder = AppDriver.screenShotFolder;
		Report.LogHTMLFolder = AppDriver.LogHTMLFolder;
		Report.ReportHeader("TA sample script");
		Global_Variable.iTcCnt = 0;
		Global_Variable.iTcCnt++;
		
		try {
			System.out.println("Test"+Global_Variable.TC_TD_Path);
			ExcelLib.setExcelFile(Global_Variable.TC_TD_Path);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		try{
			
			String sUSername = "9095373600",sPassword="88888888",sTransfer_amt="5",sNote="Test",sPasscode="1234",sFrnd_name="test06";
			System.out.println("Main script");
			
			API.EnterTextFeild(driver, OR.txtUSername, "Username", sUSername);
			
			/*WebElement next = OR.lstNext.get(0);
			API.ClickFeild(driver,next, "Next button");*/
			
			API.ClickFeild(driver,OR.btnNext,"Next");
	
			API.EnterTextFeild(driver, OR.txtPassword, "Password", sPassword);
			
			API.ClickFeild(driver,OR.btnNext,"Next");
			
			API.fnWait(driver, OR.lnkTransfer, "Transfer link", 20);
			API.ClickFeild(driver, OR.lnkTransfer, "Transfer link");
			API.EnterTextFeild(driver, OR.txtAmount, "Transfer amount",sTransfer_amt );
			API.EnterTextFeild(driver, OR.txtNote, "Note ", sNote);
			API.EnterTextFeild(driver, OR.txtPasscode, "Passcode",sPasscode );
			API.ClickFeild(driver, OR.btnShare, "Share with Presto user");
			API.EnterTextFeild(driver, OR.txtSrchFrnd, "Search friend", sFrnd_name);
			API.ClickFeild(driver, OR.lblFrnd," Friend listed");
			API.ClickFeild(driver, OR.btnTransfer, "Transfer");
			API.ClickFeild(driver, OR.btnDone, "Done ");
			
			
		}catch(Exception e){
			Report.Fail(driver,"Exception" + e.getMessage());
			e.printStackTrace();
		}finally{
			Report.writeToLog(URL);
		}
	
		}
	
	
	
}

