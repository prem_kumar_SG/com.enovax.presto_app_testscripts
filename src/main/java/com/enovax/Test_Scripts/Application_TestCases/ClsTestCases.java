package com.enovax.Test_Scripts.Application_TestCases;

import com.enovax.Test_Scripts.Global_Variable;
import com.enovax.Test_Scripts.Application_TestSteps.ClsTestSteps;
import com.enovax.Test_Scripts.Application_Utilities.ExcelLib;

/*

S.No  Class name		Created by	Created Date	Purpose												Modifed By	Modified Date
************************************************************************************************************************************
1.	  ClsTestCases		Prem kumar	25/May/2017		To maintains the scripts for all test cases
**/

public class ClsTestCases {
	
	/**
	 Method name : fnLogin
	 Created By : Prem kumar
	 Created Date : 26/May/2017
	 Purpose : To login into application
	 Modifed By :
	 Modified Date :
	 */
	public static boolean fnLogin()
	{
		try {
			String sUsername = ExcelLib.getCellData(1, 2, Global_Variable.sSht_TestData);
			String sPassword = ExcelLib.getCellData(2, 2, Global_Variable.sSht_TestData);
			
			ClsTestSteps.fnUsername(sUsername);
			ClsTestSteps.fnPassword(sPassword);
			ClsTestSteps.fnclickLogin();
			
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			
			return false;
		}
	}
	
}
	
